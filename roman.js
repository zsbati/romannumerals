/*Convert the given number into a roman numeral.
All roman numerals answers should be provided in upper-case.*/

function convertToRoman(num) {
 let n = num; //a copy, not to alter the original;
 let out = []; //will contain the output;
 //determining the thousands;
 let thousands = Math.floor(n/1000); 
 n = n % 1000; //reset
 let sequence = '';
 if (thousands !== 0){
   for (let i=0; i<thousands; i++){
     sequence += 'M';
   }
 }
 out.push(sequence);
 sequence = ''; //reset;
 //determining the hudreds;
 let hundreds = Math.floor(n/100);
 n = n % 100;
 if (hundreds !== 0){
   if (hundreds === 9){
     sequence = 'CM';
   }
   if (hundreds === 8){
     sequence = 'DCCC';
   }
   if (hundreds === 7){
     sequence = 'DCC';
   }
   if (hundreds === 6){
     sequence = 'DC';
   }
   if (hundreds === 5){
     sequence = 'D';
   }
   if (hundreds === 4){
     sequence = 'CD';
   }
   if (hundreds>=1 && hundreds<= 3){
     for (let i=1; i<=hundreds; i++){
       sequence += 'C';
     }
   }
 }
 out.push(sequence);
 sequence = ''; //reset;

 //now the tens;
 let tens = Math.floor(n/10);
 n = n % 10; 
 if (tens !== 0){
   if (tens === 9){
     sequence = 'XC';
   } else if (tens === 8){
     sequence = 'LXXX';
   } else if (tens === 7){
     sequence = 'LXX';
   }
   else if (tens === 6){
     sequence = 'LX';
   } else if (tens === 5){
     sequence = 'L';
   } else if (tens === 4){
     sequence = 'XL';
   } else {
     if (tens>=1 && tens<= 3){
     for (let i=1; i<=tens; i++){
       sequence += 'X';
     }
   }
   }
 }

 out.push(sequence); //register
 sequence = ''; //reset;

//now the units it is n, taken care of; 
 if (n !== 0){
   if (n === 9){
     sequence = 'IX';
   } else if (n === 8){
     sequence = 'VIII';
   } else if (n === 7){
     sequence = 'VII';
   }
   else if (n === 6){
     sequence = 'VI';
   } else if (n === 5){
     sequence = 'V';
   } else if (n === 4){
     sequence = 'IV';
   } else {
     if (n>=1 && n<= 3){
     for (let i=1; i<=n; i++){
       sequence += 'I';
     }
   }
   }
 }

  out.push(sequence); //register

 return out.join('');
}

//convertToRoman(3661);
//convertToRoman(501); // should return "DI"

//convertToRoman(649); // should return "DCXLIX"

//convertToRoman(798); // should return "DCCXCVIII"

//convertToRoman(891); // should return "DCCCXCI"

//convertToRoman(1000); // should return "M"

//convertToRoman(1004); // should return "MIV"

//convertToRoman(1006); // should return "MVI"

//convertToRoman(1023); // should return "MXXIII"

//convertToRoman(2014); //should return "MMXIV"

convertToRoman(2021); // should return "MMMCMXCIX"
